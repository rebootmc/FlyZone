package me.eduproard.FlyZone;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.Rel;
import com.massivecraft.factions.util.RelationUtil;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Main extends JavaPlugin implements Listener {
	
	public List<UUID> FallDefense = new ArrayList<>();
	
	public enum ZoneType {
		Faction, WorldGuard;
	}
	
	public void onEnable() {
		saveDefaultConfig();
		boolean Faction = FACTION();
		boolean WorldGuard = WORLDGUARD();
		Bukkit.getPluginManager().registerEvents(this, this);
		getLogger().info("ENABLED! Faction Status: " + Faction + "   |   WorldGuard Status: " + WorldGuard + " .");
	}
	
	/*
	 * EVENTS
	 */
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if ((isDifferentLocation(event.getFrom(), event.getTo())) && (player.isFlying())
				&& (!player.hasPermission("flyfix.bypass"))) {
			if(CanFly(ZoneType.WorldGuard, player, event.getTo()) == true) {
				return;
			}
			if(CanFly(ZoneType.Faction, player, event.getTo()) == true) {
				return;
			}
			player.setFlying(false);
			player.setAllowFlight(false);
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("denyFlightMessage")));
			final UUID playerID = player.getUniqueId();
			FallDefense.add(playerID);
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				@Override
				public void run() {
					FallDefense.remove(playerID);
				}
			}, 80L);
		}
	}

	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		if ((event.getCause() == EntityDamageEvent.DamageCause.FALL)
				&& (FallDefense.contains(event.getEntity().getUniqueId()))) {
			event.setCancelled(true);
			FallDefense.remove(event.getEntity().getUniqueId());
		}
	}
	/*
	 * UTILS
	 */
	public boolean FACTION() {
		return Bukkit.getPluginManager().isPluginEnabled("Factions");
	}

	public boolean WORLDGUARD() {
		return Bukkit.getPluginManager().isPluginEnabled("WorldGuard");
	}
	
	public boolean CanFly(ZoneType Type, Player Player, Location Location) {
		if(Type == ZoneType.Faction) {
			if(FACTION() == true) {
				FileConfiguration config = this.getConfig();
				Faction faction = Board.getFactionAt(Location);
				FPlayer fPlayer = FPlayers.i.get(Player);
				Rel frel = RelationUtil.getRelationOfThatToMe(faction, fPlayer);
				if(Player.hasPermission("flyzone.donator")) {
					return config.getBoolean("ally") && frel.name().equalsIgnoreCase("ally") ? true : (config.getBoolean("enemy") && frel.name().equalsIgnoreCase("enemy") ? true : (config.getBoolean("neutral") && frel.name().equalsIgnoreCase("neutral") ? true : (config.getBoolean("truce") && frel.name().equalsIgnoreCase("truce") ? true : faction.isNone() || faction.getOnlinePlayers().contains(Player))));
				} else {
					if(ChatColor.stripColor(faction.getTag()).equalsIgnoreCase("wilderness")) {
						return false;
					}
					if(frel.name().equalsIgnoreCase("member") || frel.name().equalsIgnoreCase("recruit") || frel.name().equalsIgnoreCase("officer") || frel.name().equalsIgnoreCase("leader")) {
						return true;
					}
				}
			}
		} else if(Type == ZoneType.WorldGuard) {
			if(WORLDGUARD() == true) {
				WorldGuardPlugin Plugin = WorldGuardPlugin.inst();
				RegionManager Manager = Plugin.getRegionManager(Player.getWorld());
				ApplicableRegionSet RegionSet = Manager.getApplicableRegions(Location);
				for(ProtectedRegion Reg : RegionSet) {
					if(Player.hasPermission("flyzone.donator")) {
						if(getConfig().getStringList("AllowedRegions").contains(Reg.getId())) {
							return true;
						}
					}
				}
			}
		} else {return false;}
		return false;
	}
	
	
	public static boolean isDifferentLocation(Location from, Location to) {
		return (from.getX() != to.getX()) || (from.getY() != to.getY()) || (from.getZ() != to.getZ());
	}
}